add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
#################################
###Download agent
wget "https://$serverhost/api/v1/agent/packages/types/msi" -outfile "C:/liagent.msi"
###Install agent
C:/liagent.msi /quiet SERVERHOST=$serverhost
###Wait until services running
Start-Sleep 5
###Verify agent running
$liservicename = Get-Service -Name *LogInsight*
if ($liservicename.Status -ne "Running"){
	Write-Output "Log Insight agent is not running."
	exit 1}
else{
	Write-Output "Log Insight agent is installed!"
	del C:/liagent.msi
	exit 0}